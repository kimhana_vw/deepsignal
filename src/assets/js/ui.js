const html = document.querySelector('html');
const body = document.querySelector('body');

var Common = {
	init : function(){
		Common.scrolling();
		window.addEventListener('mousewheel', Common.scrolling);
		window.addEventListener('touchmove', Common.scrolling);
		$(window).scroll(function(){
			Common.scrolling();
		});
	},
	scrolling : function(e){
		var scrollTop = $(window).scrollTop();
		var gnbTop = $('#header').height();
		gnbTop = Number(gnbTop);

		if(scrollTop > 50){
			$('html').addClass('is-scrolled');
		}else{
			$('html').removeClass('is-scrolled');
		}

		//scrollbar bottom check
		if($(window).scrollTop() + $(window).height() > $(document).height() - 50) {
			$('html').addClass('is-bottom');
		}else{
			$('html').removeClass('is-bottom');
		}
	},
};

var Header = {
	init : function(){
		Header.alarm();
		Header.my();
		Header.myinfo();
		Header.fullscreen();
	},
	alarm : function(e){
		$('.alarm a').on('click',function(){
			$('html').addClass('alarm-layer-show');
			$('.layer-wrap').addClass('active');
		});
		$('#js-btn-alarm-close, .alarm-layer .dim').on('click',function(){
			$('html').removeClass('alarm-layer-show');
			$('.layer-wrap').removeClass('active');
		});
	},
	my : function(e){
		$('.mypage a').on('click',function(){
			$('html').addClass('my-layer-show');
		});
		$('.my-info-top .layer-btn-close').on('click',function(){
			$('html').removeClass('my-layer-show');
		});
	},
	myinfo : function(e){
		$('#js-btn-myinfo').on('click',function(){
			$('html').removeClass('my-layer-show');
			$('html').addClass('myinfo-layer-show');
		});
		$('#js-btn-myinfo-close').on('click',function(){
			$('html').removeClass('myinfo-layer-show');
		});
	},
	fullscreen : function(e){
		$('.fullscreen > a').on('click',function(e){
			e.preventDefault();
			console.log(1)
			if(!$('html').hasClass('is-fullscreen')){
				openFullScreenMode();
				$('html').addClass('is-fullscreen');
				$(this).find('i').text('fullscreen_exit');
			}else{
				closeFullScreenMode();
				$('html').removeClass('is-fullscreen');
				$(this).find('i').text('fullscreen');
			}
		});
		var docV = document.documentElement;
        //전체화면 설정
		function openFullScreenMode() {
			if (docV.requestFullscreen)
				docV.requestFullscreen();
			else if (docV.webkitRequestFullscreen)
				docV.webkitRequestFullscreen();
			else if (docV.mozRequestFullScreen)
				docV.mozRequestFullScreen();
			else if (docV.msRequestFullscreen)
				docV.msRequestFullscreen();
        }
        //전체화면 해제
        function closeFullScreenMode() {
            if (document.exitFullscreen)
            document.exitFullscreen();
            else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
            else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
            else if (document.msExitFullscreen)
            document.msExitFullscreen();
        }
	}
}

var Sidebar = {
	init : function(){
		Sidebar.gnb();
		Sidebar.information();
	},
	gnb : function(e){
		$('.gnb .dep1 > li > a').on('mouseenter focus', function(e){
			$(this).parent().siblings().find('a').removeClass('hover');
			$(this).addClass('hover');
		});
		$('.gnb').on('mouseleave', function(e){
			$('.gnb .hover').removeClass('hover');
		});
	},
	information : function(e){
		var btnInfo = $('.btn-information');
		$(btnInfo).on('click', function(e){
			$(this).toggleClass('active');
		});
		$(btnInfo).on('focus', function(e){
			$('.gnb a').removeClass('hover');
		});
		$(document).on('click', function(e){
			if ($('.btn-information-wrap').has(e.target).length === 0){
				$(btnInfo).removeClass('active');
			}
		});
	},
}

var eventCommon = function(){
	$('[aria-label="like"]').on('click',function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$(this).attr('aria-pressed',false)
		}else{
			$(this).addClass('active');
			$(this).attr('aria-pressed',true)
		}
	});

	$('.tooltip-menu a').on('click', function(e){
		if($(this).parent().hasClass('has-treeview')){
			e.preventDefault();
			$(this).parent().addClass('show');
		}
	});
}

Common.init();
Header.init();
Sidebar.init();
eventCommon();

$(document).ready(function() {
	$(".overflow-y-scroll").mCustomScrollbar({
		theme:"dark"
	});
});
